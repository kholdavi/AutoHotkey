;This script simply starts or closes Windows 7's Sticky Notes

IfWinExist ahk_class Sticky_Notes_Note_Window 
{   
	WinClose ahk_class Sticky_Notes_Note_Window
	process, close, StikyNot.exe
} else   {
	Run C:\Windows\Explorer.exe %windir%\system32\StikyNot.exe
	Sleep, 200
	WinActivate, Sticky Note
}
exitApp