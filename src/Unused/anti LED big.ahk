Swidth := A_ScreenWidth
Sheight := A_ScreenHeight

xPos := 50, yPos := 60
Width :=200, Height := 80
ToggleWidth := 30, ToggleHeight := 30
timeOn = 1000
speed = 8




SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
#Include, Gdip.ahk
#SingleInstance, Force
#NoEnv
SetBatchLines, -1

; Start gdi+
If !pToken := Gdip_Startup()
{
	MsgBox, 48, gdiplus error!, Gdiplus failed to start. Please ensure you have gdiplus on your system
	ExitApp
}
OnExit, Exit





Gui, 1: -Caption +E0x80000 +LastFound +AlwaysOnTop +ToolWindow +OwnDialogs		; Create a layered window (+E0x80000 : must be used for UpdateLayeredWindow to work!) that is always on top (+AlwaysOnTop), has no taskbar entry or caption
Gui, 1: Show, NA																; Show the window
hwnd1 := WinExist()																; Get a handle to this window we have created in order to update it later
Gui, Show, Hide

hbm := CreateDIBSection(Width, Height)					; Create a gdi bitmap with width and height of what we are going to draw into it. This is the entire drawing area for everything
hdc := CreateCompatibleDC()								; Get a device context compatible with the screen
obm := SelectObject(hdc, hbm)							; Select the bitmap into the device context
G := Gdip_GraphicsFromHDC(hdc)							; Get a pointer to the graphics of the bitmap, for use with drawing functions
;Gdip_SetSmoothingMode(G, 4)


;;;;; create background
pBrushEnabled := Gdip_BrushCreateSolid(0xee111111)
Gdip_FillRoundedRectangle(G, pBrushEnabled, 0, 0, Width, Height, 10)
Gdip_DeleteBrush(pBrushEnabled)

;;;;; create texts
Font = Calibri
Options1 = x5p  y0 w30p Centre cffffffff r4 s20
Options2 = x35p y0 w30p Centre cffffffff r4 s20
Options3 = x65p y0 w30p Centre cffffffff r4 s20
Gdip_TextToGraphics(G, "Num"    , Options1, Font, Width, Height)
Gdip_TextToGraphics(G, "Caps"   , Options2, Font, Width, Height)
Gdip_TextToGraphics(G, "Scroll" , Options3, Font, Width, Height)

UpdateLayeredWindow(hwnd1, hdc, xPos, yPos, Width, Height)


;SelectObject(hdc, obm)		; Select the object back into the hdc
;DeleteObject(hbm)			; Now the bitmap may be deleted
;DeleteDC(hdc)				; Also the device context related to the bitmap may be deleted
;Gdip_DeleteGraphics(G)		; The graphics may now be deleted





~$NumLock::
~$CapsLock::
~$ScrollLock::
	send {%A_ThisHotkey% d}
	KeyWait, %A_ThisHotkey%
	gosub, reloadLED 
return


Return

;#######################################################################
	
reloadLED: 
	Gui, 1: Show, NA

	NumState := GetKeyState("NumLock", "T")
	CapsState := GetKeyState("CapsLock", "T")
	ScrollState := GetKeyState("ScrollLock", "T")


	pBrushEnabled := Gdip_BrushCreateSolid(0xff3399cc) 
	pBrushDisabled := Gdip_BrushCreateSolid(0xff333333) 
	Gdip_FillRectangle(G, ((NumState    = 1) ? (pBrushEnabled) : (pBrushDisabled)), Width/100*20-ToggleWidth/2, 30, 30, 30)
	Gdip_FillRectangle(G, ((CapsState   = 1) ? (pBrushEnabled) : (pBrushDisabled)), Width/100*50-ToggleWidth/2, 30, 30, 30)
	Gdip_FillRectangle(G, ((ScrollState = 1) ? (pBrushEnabled) : (pBrushDisabled)), Width/100*80-ToggleWidth/2, 30, 30, 30)
	Gdip_DeleteBrush(pBrushEnabled)
	Gdip_DeleteBrush(pBrushDisabled)

	UpdateLayeredWindow(hwnd1, hdc, xPos, yPos, Width, Height)

	SetTimer,timerLED, %timeOn% 
	SetTimer,timerFADE, off 
Return 

;#######################################################################

timerFADE:
	alpha := alpha - speed
	if (alpha > 0) {
		UpdateLayeredWindow(hwnd1, hdc, xPos, yPos, Width, Height, alpha)
	} else {
		SetTimer,timerFADE, off 
		Gui, Show, Hide
	}
return

timerLED: 
	SetTimer,timerLED, off 
	alpha = 255
	SetTimer,timerFADE, 10
return


;#######################################################################

Exit:
	Gdip_Shutdown(pToken)
	ExitApp
Return