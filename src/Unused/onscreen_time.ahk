#include gdip.ahk

#NoEnv
DetectHiddenWindows On
SetTitleMatchMode 2
OnExit gone_away

delta:=A_Now
delta -=A_NowUTC,seconds

Menu, Tray,add,Hide/Show , toggle_hide
Menu, Tray,default,Hide/Show
Menu,Tray ,Click, 1
Menu, Tray,add
Menu, Tray,add,Set Clock Color ,set_clockcolor
Menu, Tray,add,Set background opacity ,set_opacity
Menu, Tray,add
Menu, Tray,add,Save Position ,save_position
Menu, Tray,add,Set Click Through ,click_through
Menu, Tray,add,Set AlwaysOnTop ,AlwaysOnTop 
Menu, Tray,add,Set hotkeys off ,hotkey_off
Menu, Tray,add
Menu, Tray,add,Exit ,gone_away
Menu, Tray,NoStandard

;---------------- hotkey alt+o menu -----------------
Menu, MyMenu,add,Hide/Show , toggle_hide
Menu, MyMenu,default,Hide/Show
Menu, MyMenu,add
Menu, MyMenu,add,Set Clock Color ,set_clockcolor
Menu, MyMenu,add,Set background opacity ,set_opacity
Menu, MyMenu,add
Menu, MyMenu,add,Save Position ,save_position
Menu, MyMenu,add,Set Click Through ,click_through
Menu, MyMenu,add,Set AlwaysOnTop ,AlwaysOnTop 
Menu, MyMenu,add,Set hotkeys off ,hotkey_off
Menu, MyMenu,add
Menu, MyMenu,add,Exit ,gone_away
Menu, MyMenu,NoStandard
;---------------- alt+o menu -----------------


;-------------- create ini file -------------- 
IfNotExist, i_time.ini
{
IniWrite,cffE2E2E2, i_time.ini, parameters, clock_color
IniWrite,100, i_time.ini, parameters, positionx
IniWrite,100, i_time.ini, parameters, positiony
IniWrite,A3, i_time.ini, parameters, opacity     ;these are hex values!!!
IniWrite,0, i_time.ini, parameters, alwaysontop
IniWrite,0, i_time.ini, parameters, click_through
}
;-------------- create ini file -------------- 





;-------------- get default values -------------- 
IfExist, i_time.ini
{
IniRead,clock_color, i_time.ini, parameters, clock_color,cff3999E8
IniRead,posx, i_time.ini, parameters, positionx,100
IniRead,posy, i_time.ini, parameters, positiony,100
IniRead,opacity_value_hex, i_time.ini, parameters, opacity,A8
IniRead,alwaysontop, i_time.ini, parameters, alwaysontop,0
IniRead,click_through, i_time.ini, parameters, click_through,0
}

size=s24
background_color=0x%opacity_value_hex%000000
display_size:=size
 


;-------------- get default values -------------- 


If !pToken := Gdip_Startup()
{
	MsgBox, 48, gdiplus error!, Gdiplus failed to start. Please ensure you have gdiplus on your system
	ExitApp
}
Font = Arial
If !hFamily := Gdip_FontFamilyCreate(Font)
{
   MsgBox, 48, Font error!, The font you have specified does not exist on the system
   ExitApp
}
Gdip_DeleteFontFamily(hFamily)




Gui, 3:  +E0x80000 +LastFound +OwnDialogs +Owner  -caption 
Gui, 3: Show,w140 h35 x0 y0 NA,i_time
winget,hwnd1,ID,i_time

WinWidth =140
WinHeight :=35
hbm := CreateDIBSection(WinWidth, WinHeight), hdc := CreateCompatibleDC(), obm := SelectObject(hdc, hbm)
G := Gdip_GraphicsFromHDC(hdc)
pBrush := Gdip_BrushCreateSolid(background_color)
Gdip_SetSmoothingMode(G, 4)
Gdip_FillRoundedRectangle(G, pBrush, 0, 0, WinWidth, WinHeight,10)



OnMessage(0x201, "WM_LBUTTONDOWN")
WinMove, i_time, , %posX%, %posY% 
settimer,get_realtime,900

;-------------- set initial conditions

if alwaysontop
Gosub, alwaysontop

if click_through
Gosub, click_through

;-------------- set initial conditions
return




click_through:
Menu, tray,togglecheck,Set Click Through
Menu, MyMenu,togglecheck,Set Click Through
status:=!status
if status
{
WinGetPos, temp_x,temp_y,,,ahk_id %hwnd1%
Gui, 3:destroy
Gui, 3:  +E0x80000 +LastFound +OwnDialogs +Owner  -caption +E0x20
Gui, 3: Show,w140 h35 x%temp_x% y%temp_y% NA,i_time
winget,hwnd1,ID,i_time
if toggle_ontop
WinSet, alwaysontop,on,ahk_id %hwnd1%
}
else
{
WinGetPos, temp_x,temp_y,,,ahk_id %hwnd1%
Gui, 3:destroy
Gui, 3:  +E0x80000 +LastFound +OwnDialogs +Owner  -caption 
Gui, 3: Show,w140 h35 x%temp_x% y%temp_y% NA,i_time
winget,hwnd1,ID,i_time
if toggle_ontop
WinSet, alwaysontop,on,ahk_id %hwnd1%
}
return 
;-------------- alwaysontop -------------- 
alwaysontop:
toggle_ontop:=!toggle_ontop
menu,tray,togglecheck,set alwaysontop
menu,MyMenu,togglecheck,set alwaysontop
if toggle_ontop
WinSet, alwaysontop,on,ahk_id %hwnd1%
else
WinSet, alwaysontop,off,ahk_id %hwnd1%
return
;-------------- alwaysontop -------------- 

set_opacity:
opacity_h:="0x"  opacity_value_hex
setformat,integer,d
opacity_display:=floor(opacity_h*100/255)
setformat,integer,hex


InputBox, opacity_value_dec,Set Transparency 0-100,transparency level can be set `nvalue =100 is dark ,,200,150,,,,20,%opacity_display%

if opacity_value_dec not between 0 and 100
{
msgbox error value  %opacity_value_dec% not between 0-100
return
}
setformat,integer,hex
op:=floor(opacity_value_dec*2.5)+1
StringTrimLeft, opacity_value_hex,op,2
IniWrite,%opacity_value_hex%, i_time.ini, parameters, opacity
setformat,integer,d
Gdip_DeleteBrush(pBrush)
background_color=0x%opacity_value_hex%000000
pBrush := Gdip_BrushCreateSolid(background_color)
return 
;-------------- enable dragwindow -------------- 
WM_LBUTTONDOWN()
{
	PostMessage, 0xA1, 2
}
return
;-------------- enable dragwindow -------------- 

;-------------- hotkeys ------------------ 
!i::  ;hide/show hotkey
Gosub, toggle_hide
return
~rbutton::
MouseGetPos, ,,hwnd2
if (hwnd2=hwnd1)
{
toggle=1
WinHide, ahk_id %hwnd1%
}
return
!o::
Menu, MyMenu,show
return
;-------------- hotkeys off -------------- 

hotkey_off:
Menu, tray,togglecheck,Set hotkeys off
Hotkey, !o,toggle

return
;-------------- hotkeys off-------------- 
toggle_hide:
toggle:=!toggle
if toggle
WinHide, ahk_id %hwnd1%
Else, 
WinShow, ahk_id %hwnd1%
return 

get_realtime:
FormatTime, displaytext,,H : mm : ss
display_color=%clock_color%
display_size=s18
gosub write_display_text_clock
return
;-------------- write display text -------------- 

write_display_text_clock:
Gdip_SetCompositingMode(G, 1)
Gdip_FillRoundedRectangle(G, pBrush, 0, 0, WinWidth, WinHeight,5)
Gdip_SetCompositingMode(G, 0)
Options = x0 y6 w140 %display_color%  Center  %display_size%
Gdip_TextToGraphics(G, displaytext, Options, Font, winWidth, winHeight)
UpdateLayeredWindow(hwnd1, hdc)
return

;-------------- write display text -------------- 
set_clockcolor:
newcolor:=ChooseColorA( 0x2255ff)
if (newcolor<>"")
clock_color=cff%newcolor%
IniWrite, %clock_color%,i_time.ini,parameters,clock_color
return

save_position:
WinGetPos, posx,posy,,,ahk_id %hwnd1%
IniWrite, %posx%,i_time.ini,parameters,positionx
IniWrite, %posy%,i_time.ini,parameters,positiony
return 


FormatSeconds(NumberOfSeconds) 
{
    time = 19990101 
    time += %NumberOfSeconds%, seconds
    FormatTime, mmss, %time%, mm : ss
    hours:=NumberOfSeconds//3600
    hours:=(hours ? hours : "00")
    return  hours " : " mmss  
}

gone_away:
SelectObject(hdc, obm)
DeleteObject(hbm)
DeleteDC(hdc)
Gdip_DeleteGraphics(G)
Gdip_DeleteBrush(pBrush)
Gdip_Shutdown(pToken)
ExitApp


;  By SKAN code for choosing color http://www.autohotkey.com/forum/topic59534.html

ChooseColorA( CR=0x0, hWnd=0x0, X=25, Y=25, Title=0, CustomColors=0, RGB=1 ) {

 Static CC, Color = "000000", S22 = "                      ", ATOU = "MultiByteToWideChar"
 If ! ( VarSetCapacity( CC ) ) {
 CCD =
 ( LTrim Join
   24ZV47ZV8N8J808N8H8H8J808HC0C0CH80808HFFMFFIFFFFMFFGFFGFFIFFFFGFFFFFFGC020C88G8K1BH2K2A
   01B8O43G43G68G6FG6FG73G65G43G6FG6CG6FG72G41G2H2H2H2H2H2H2H2H2H2H2H2H2H2H2H2H2H2H2M8G4DG
   53G2H53G68G65G6CG6CG2H44G6CG67P25O4H4G8CH9GFFFFFFFF82G26G42G61G73G69G63G2H63G6FG6CG6FG7
   2G73G3ATBH35O4HEG8CG56GDG2FFFF82X25O4G6AG8CH9GFFFFFFFF82G26G43G75G73G74G6FG6DG2H63G6FG6
   CG6FG72G73G3APBH35O4G74G8CG1CGD102FFFF82X35O4G96G8CHEGCF02FFFF8H26G44G65G66G69G6EG65G2H
   43G75G73G74G6FG6DG2H43G6FG6CG6FG72G73G2H3EG3ET1H35O4GA6G2CHEH1GFFFF8H4FG4BX35N34GA6G2CH
   EH2GFFFF8H43G61G6EG63G65G6CX35N64GA6G2CHEHE04FFFF8H26G48G65G6CG7QB1H5N98H4G76G74GC602FF
   FF82TB1H5N180104H8G74GBE02FFFF82TB1H5N98G7CG28G1AGC502FFFF82X35N2C01C8H4HEGC902FFFF8H26
   G6FT2H25N98G97G14H9GDA02FFFF82G43G6FG6CG6FG72T25NACG97G14H9GDB02FFFF82G7CG53G26G6FG6CG6
   9G64P2H25NC2G7EG14H9GD302FFFF82G48G75G26G65G3AS835ND8G7CG12HCGBF02FFFF81T2H25NC2G8CG14H
   9GD402FFFF82G26G53G61G74G3AS835ND8G8AG12HCGCG2FFFF81T2H25NC2G9AG14H9GD502FFFF82G26G4CG7
   5G6DG3AS835ND8G98G12HCGC102FFFF81T2H25NF3G7EG18H9GD602FFFF82G26G52G65G64G3AS835OD017CG1
   2HCGC202FFFF81T2H25NF3G8CG18H9GD702FFFF82G26G47G72G65G65G6EG3AS835OD018AG12HCGC302FFFF8
   1T2H25NF3G9AG18H9GD802FFFF82G42G6CG26G75G65G3AW835OD0198G12HCGC402FFFF81X35N98GA6G8EHEG
   C802FFFF8H26G41G64G64G2H74G6FG2H43G75G73G74G6FG6DG2H43G6FG6CG6FG72G73O
 )
 Loop 20
  StringReplace,CCD,CCD,% Chr(70+21-A_Index),% SubStr("000000000000000000000",A_Index),All
 Loop % VarSetCapacity(CC,StrLen(CCD)//2,0)
  NumPut( "0x" . SubStr(CCD, 2*A_Index-1,2),CC,A_Index-1,"Char" )
 }
 Numput( &CC+100,CC,8 ), NumPut( &CC+36,CC,16 )
 IfNotEqual,CustomColors,0, Loop, Parse, CustomColors, |
 _ := (A_LoopField<>"" && A_Index<17) ? NumPut("0x" A_LoopField,CC,36+(4*(A_Index-1))) : 0
 Title ? DllCall( ATOU, Int,0,Int,0, Str,Title S22, UInt,22, UInt,&CC+122, UInt,44 ) : 0
 NumPut(Y,CC,112,"UShort"), NumPut(X,CC,110,"UShort"), NumPut(hWnd,CC,4)
 WinExist( "ahk_id" hWnd ) ? NumPut(0,CC,104) : 0
 RGB ? NumPut((((CR&0xFF)<<16)|(CR&0xFF00)|((CR&0xFF0000)>>16)),CC,12) : NumPut(CR,CC,12)
 If ! DllCall( "comdlg32\ChooseColorA", UInt,&CC ) || ErrorLevel
      Return
 DllCall( "msvcrt\sprintf", Str,Color, Str,"%06X", UInt, RGB ? ( (((CR:=Numget(CC,12) )
  &0xFF)<<16)|(CR&0xFF00)|((CR&0xFF0000)>>16)) : Numget(CC,12) )
Return Color
}




















































