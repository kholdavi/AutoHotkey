ICON_PATH := A_ScriptDir . "\Icons\cog-multi-size.ico"
Menu TRAY, Icon, %ICON_PATH%

SendMode Input  ;

;-------------------------------------------------
;--------------- Replace Caps lock with ctrl
;-------------------------------------------------
; Toggle caps lock with some key combination?
;#Capslock::Capslock
;+Capslock::Capslock
;Capslock::Ctrl
Capslock::LCtrl


;-------------------------------------------------
;--------------- Toggle always on top
;-------------------------------------------------
#t::  Winset, Alwaysontop, , A

;-------------------------------------------------
;--------------- Auto correct
;-------------------------------------------------
::dont::don't
::didnt::didn't
::doesnt::doesn't
::cant::can't
::couldnt::couldn't
::wont::won't
::wouldnt::wouldn't
::hasnt::hasn't
::hadnt::hadn't
::shouldnt::shouldn't
::arent::aren't
::isnt::isn't
::werent::weren't
::wasnt::wasn't

::ahve::have
::liek::like


;-------------------------------------------------
;--------------- Convert Hanja key into control
;-------------------------------------------------
SC11D:: RCtrl
;SC138:: RAlt

;-------------------------------------------------
;--------------- Ctrl backspace works in Notepad
;-------------------------------------------------
#IfWinActive ahk_class CabinetWClass ; File Explorer
    ^Backspace::
#IfWinActive ahk_class Notepad
    ^Backspace:: Send ^+{Left}{Backspace}
#IfWinActive

;-------------------------------------------------
;--------------- Ctrl delete works in Notepad
;-------------------------------------------------
#IfWinActive ahk_class CabinetWClass ; File Explorer
    ^Delete::
#IfWinActive ahk_class Notepad
    ^Delete:: Send ^+{Right}{Backspace}
#IfWinActive

;-------------------------------------------------
;--------------- Close CMD with shortcut
;-------------------------------------------------
#IfWinActive ahk_class ConsoleWindowClass
!F4::WinClose, A
^w::WinClose, A
^v:: send !{Space}EP
^Up:: Send {WheelUp}
^Down:: Send {WheelDown}
#IfWinActive


;-------------------------------------------------
;--------------- Close Calculator with shortcut
;-------------------------------------------------
#IfWinActive Calculator
^w::WinClose, A
#IfWinActive

;-------------------------------------------------
;--------------- Run apps via shortcut
;-------------------------------------------------
#s:: run, C:\Users\DAVE\Dropbox
#a:: run, C:\
#n:: run notepad
#`:: run cmd
#m::
IfWinExist Calculator
  WinActivate Calculator
else
  Run calc
return

;-------------------------------------------------
;--------------- Volume control on mouse wheel
;-------------------------------------------------
#wheelup:: Send {Volume_Up}
#wheeldown:: Send {Volume_Down}
#MButton:: Send {Volume_Mute}
#Volume_Down:: Send {Volume_Down}

;-------------------------------------------------
;--------------- Disable printing in Notepad
;-------------------------------------------------
$^p::
    IfWinActive ahk_class Notepad
        return
    Send ^p
return

;-------------------------------------------------
;--------------- Search google from everywhere
;-------------------------------------------------
;*#g::
;send ^c 
;sleep 50
;run http://www.google.com/search?q=%Clipboard%
;return

